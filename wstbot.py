import os
import logging
import time
import sleekxmpp
from util import escape, WstbotConfig
from constants import MEDIA_DB_PATH, STREAM_LOG_FORMAT, CONFIG_FILE
from message_handling import MessageHandler

# from sleekxmpp import ClientXMPP
# from sleekxmpp.exceptions import IqError, IqTimeout

class Wstbot(sleekxmpp.ClientXMPP):

    def __init__(self, config):
        super().__init__(config.conn_account, config.conn_password)

        self.config = config

        self.room = config.conn_room
        self.nick = config.conn_nick

        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("groupchat_message", self.muc_message)
        self.add_event_handler("session_end", self.session_end)
        self.add_event_handler("disconnected", self.disconnected)
        self.add_event_handler("muc::{0}::got_online".format(self.room), self.muc_online)

        self.register_plugin("xep_0030") # Service Discovery
        self.register_plugin("xep_0045") # Multi-User Chat
        self.MUC = self.plugin["xep_0045"]
        self.register_plugin("xep_0199") # XMPP Ping

        self.msg_handler = MessageHandler()

    def init_checks(self):
        if not os.path.exists(MEDIA_DB_PATH):
            logging.error("Path does not exist: %s. Did you forget to run the setup?", os.path.abspath(MEDIA_DB_PATH))
            return False

        return True

    def session_start(self, event):
        #self.get_roster()
        self.send_presence()

        logging.info("joining room %s", self.room)
        self.MUC.joinMUC(self.room, self.nick, wait=True)

    def muc_online(self, presence):
        room_name = self.room_local_name(presence["muc"]["room"])
        if presence["muc"]["nick"] == self.nick: # we have joined a room
            self.send_room_message("Hi {}!".format(room_name))

    def send_room_message(self, message):
        """formatted means that it is an html message"""
        
        if message is None:
            return

        try:
            # send every line seperately
            lines = message.split("\n")
            for line in lines:
                logging.info(line)
                self.send_message(mto=self.room, mbody=line, mtype="groupchat")
                time.sleep(1)
        except Exception as ex:
            logging.warning("Could not send message %s, Error: %s", message, str(ex))
            message = "Error while trying to send a message: {}".format(escape(str(ex)))
            self.send_message(mto=self.room, mbody=message, mtype="groupchat")

    def muc_message(self, msg):
        # ignore messages from self
        if msg["mucnick"] == self.nick:
            return

        if msg["body"].startswith("!link") or msg["body"].startswith("!help"):
            media_str = "Media: {}/media".format(self.config.server_url)
            upload_str = "Upload: {}/upload".format(self.config.server_url)
            control_str = "Web interface: {}".format(self.config.web_interface_url)
            self.send_room_message("{}\n{}\n{}".format(media_str, upload_str, control_str))
        else:
            self.send_room_message(self.msg_handler.handle_message(msg["body"]))

    def room_local_name(self, room_name):
        return room_name[:room_name.find("@")]

    def disconnected(self, data):
        logging.info("disconnected")

    def session_end(self, data):
        logging.info("session end")

def main():
    # these settings will be used by sleekxmpp
    logging.basicConfig(level=logging.INFO, format=STREAM_LOG_FORMAT)

    try:
        config = WstbotConfig()
    except Exception as ex:
        logging.error("Could not parse config file %s: %s", CONFIG_FILE, str(ex))
        return

    #signal.signal(signal.SIGINT, on_sigint)

    wstbot = Wstbot(config)

    if not wstbot.init_checks():
        return

    if wstbot.connect():
        wstbot.process(block=True)
    else:
        logging.error("Unable to connect")

if __name__ == '__main__':
    main()
