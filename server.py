import os
import cherrypy
from constants import WEB_PATH
from util import WstbotConfig, get_template_content
from server_media import MediaList
from server_upload import Uploader

LOG_FILE = "server.log"
SERVE_INDEX = False

class WstbotServer:

    def __init__(self):

        # load config
        self.wstbot_config = WstbotConfig()

        cherrypy.config.update({
            "server.socket_port": self.wstbot_config.server_port,
            "server.socket_host": "0.0.0.0"
        })

        app_path = os.path.abspath(".")

        self.server_config = {
            "/":
            {
                "tools.staticdir.root": os.path.join(app_path, WEB_PATH)
            },

            "/css":
            {
                "tools.staticdir.on": True,
                "tools.staticdir.dir": "css"
            },

            "/js":
            {
                "tools.staticdir.on": True,
                "tools.staticdir.dir": "js"
            },

            "/img":
            {
                "tools.staticdir.on": True,
                "tools.staticdir.dir": "img"
            }
        }

        self.media = MediaList()
        self.upload = Uploader()

    @cherrypy.expose
    def index(self):
        if SERVE_INDEX:
            return get_template_content("index.html")
        else:
            return ""

def main():
    server = WstbotServer()
    cherrypy.quickstart(server, "/", server.server_config)

if __name__ == "__main__":
    main()
