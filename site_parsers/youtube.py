import logging
import re
from util import URL_REGEX_PREFIX, download_page_decoded
from site_parsers.site_parser import SiteParser

class Youtube(SiteParser):

    def __init__(self, *args):
        super().__init__(*args)
        self.video_id = None

    def find_info(self, url):
        # valid youtube url?
        match_normal = re.search(URL_REGEX_PREFIX + "youtube\.com/watch.*v=(\S{11})", url)
        match_short = re.search(URL_REGEX_PREFIX + "youtu\.be/(\S+)", url)
        match = match_normal or match_short
        if match is None:
            return

        self.video_id = match.group(1)
        logging.info("Found youtube video: " + self.video_id)

        # find info
        content = download_page_decoded(url)
        if content is None:
            logging.warning("Error downloading content!")
            return
        match_title = re.search('<meta property="og:title" content="(.+)"\s*>', content)
        match_duration = re.search('<meta itemprop="duration" content="PT(.+)"\s*>', content)
        if (match_title and match_duration) is None:
            logging.warning("Could not find information about the video!")
            return

        raw_title = match_title.group(1)
        title = raw_title
        logging.debug("title: {}".format(title))
        duration = match_duration.group(1)
        duration = duration.replace("M", "m ")
        duration = duration.replace("S", "s ")
        duration = duration.replace("H", "h ")

        # add starting point
        at = ""
        print(match.group(0))
        match_t = re.search("(&|#|\?)t=(?P<time>\S*?)(&.*?)?$", url)
        if match_t is not None:
            at = "(" + match_t.group("time") + ") "

        message = "{}{} :: {}".format(at, title, duration)

        return (message, raw_title)

    def find_media_info(self, url):
        if self.video_id is not None:
            return ("youtube", self.video_id)
        else:
            return None

CLASS_ = Youtube
