import logging
import json
import re
from util import URL_REGEX_PREFIX, download_page
from site_parsers.site_parser import SiteParser

class Vimeo(SiteParser):

    def __init__(self, *args):
        super().__init__(*args)
        self.video_id = None

    def find_info(self, url):
        # get video id
        match = re.match(URL_REGEX_PREFIX + 'vimeo\.com/(\S*)', url)
        if match is None:
            return

        self.video_id = match.group(1)
        logging.info("Found vimeo video: " + self.video_id)

        json_url = "http://vimeo.com/api/v2/video/{0}.json".format(self.video_id)
        json_content = download_page(json_url).decode("utf-8")
        json_data = json.loads(json_content)
        json_data = json_data[0]
    
        raw_title = json_data["title"]
        title = raw_title 
        secs = int(json_data["duration"])
        duration = str(int(secs / 60)) + "m " + str(secs % 60) + "s"

        message = "{0} :: {1}".format(title, duration)

        return (message, raw_title)

    def find_media_info(self, url):
        if self.video_id is not None:
            return ("vimeo", self.video_id)
        else:
            return None

CLASS_ = Vimeo
