import re
import logging
import sqlite3
from util import URL_REGEX_PREFIX, parse_for_url, get_modules_objects, chain_call
from constants import MEDIA_DB_PATH, PARSERS_PATH

# what kinds of links should be stored?
STORE_IMAGES = True
STORE_LINKS = True # meaning all other links
ITEMS_PER_PAGE = 15

class MessageHandler:
    def __init__(self):
        self.media = Media()
        self.parsers = get_modules_objects(PARSERS_PATH)

    def handle_message(self, msg):
        url = parse_for_url(msg)
        if url is None:
            return

        active_parser = None
        info = None
        title = None

        for p in self.parsers:
            r = p.find_info(url)
            if r is not None:
                info, title = r
                active_parser = p
                logging.debug("title: %s", title)

        if msg.strip()[-1] != "*":
            # process all urls/links
            matches = re.findall(r"(" + URL_REGEX_PREFIX + "\S+)\s*", msg)
            for url in matches:
                # store media
                if active_parser is not None:
                    mediatype, url = active_parser.find_media_info(url)
                    self.media.store_media(url, title=title, mediatype=mediatype)
                else:
                    # use the builtin media handlers
                    self.media.store_media(url, title=title)

        return info

class Media:
    """Parse for URLs that could be of interest and store them"""

    def __init__(self):
        pass
        
    def store_media(self, url, title=None, mediatype=None):
        if mediatype is None:
            # try the builtin types
            media_info = chain_call(url, [self.parse_image, self.parse_link])

            if media_info is None:
                logging.warning("media was not stored")
                return 
            mediatype, url = media_info

        if title is None:
            title = ""

        # write
        with sqlite3.connect(MEDIA_DB_PATH) as conn:
            cur = conn.cursor()
            cur.execute("insert into media (type, title, url) values (?, ?, ?)",
                    (mediatype, title, url))
            conn.commit()

    def parse_link(self, url):
        if not STORE_LINKS:
            return None

        return ("link", url)

    def parse_image(self, url):
        if not STORE_IMAGES:
            return None

        # prefix for URLs in general
        match = re.search("(" + URL_REGEX_PREFIX + r".*(\.jpeg|\.jpg|\.png|\.gif))", url, re.IGNORECASE)
        if match is None:
            return None

        logging.info("Found image url: " + url)
        return ("image", url)