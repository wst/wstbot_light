import sqlite3
import os
from constants import MEDIA_DB_PATH, DATA_PATH, FILES_PATH, BACKUP_PATH

MEDIA_TABLE = """create table media (
id integer primary key,
type text,
title text,
url text
);"""

def main():
    if not os.path.isdir(DATA_PATH):
        os.makedirs(DATA_PATH)

    if not os.path.isdir(FILES_PATH):
        os.makedirs(FILES_PATH)

    if not os.path.isdir(BACKUP_PATH):
        os.makedirs(BACKUP_PATH)

    if not os.path.isfile(MEDIA_DB_PATH):
        with sqlite3.connect(MEDIA_DB_PATH) as conn:
            cur = conn.cursor()
            cur.execute(MEDIA_TABLE)
            conn.commit()

if __name__ == "__main__":
    main()
