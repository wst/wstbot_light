import os

CONFIG_FILE = "wstbot.conf"

# log format strings
STREAM_LOG_FORMAT = "[%(module)s] (%(levelname)s) %(message)s"
FILE_LOG_FORMAT = "%(asctime)s [%(module)s] (%(levelname)s) %(message)s"

# paths
BACKUP_PATH = "backup"
DATA_PATH = "data"
MEDIA_DB_PATH = os.path.join(DATA_PATH, "media.db")
PARSERS_PATH = "site_parsers"
SERVER_CONFIG_PATH = "server.conf"
WEB_PATH = "web"
FILES_PATH = "files"