import os.path
import subprocess
import cherrypy
import time
from string import Template
from util import get_template_content, WstbotConfig
from constants import WEB_PATH

SLEEP_AMOUNT = 2

WSTBOT_COMMAND = ["python3", "wstbot.py"]
SERVER_COMMAND = ["python3", "server.py"]

class WebInterface:

    def __init__(self):
        self.wstbot_xmpp_proc = None
        self.wstbot_server_proc = None

        # load config
        self.wstbot_config = WstbotConfig()

        # apply config
        cherrypy.config.update({
            "server.socket_port": self.wstbot_config.web_interface_port,
            "server.socket_host": "0.0.0.0"
        })

        app_path = os.path.abspath(".")

        self.server_config = {
            "/":
            {
                "tools.staticdir.root": os.path.join(app_path, WEB_PATH)
            },

            "/css":
            {
                "tools.staticdir.on": True,
                "tools.staticdir.dir": "css"
            }
        }

    @cherrypy.expose
    def index(self, toggle_wstbot=False, toggle_server=False):

        # start or stop processes

        if toggle_wstbot:
            self.toggle_wstbot_xmpp()
        elif toggle_server:
            self.toggle_server()

        # show page
        template = Template(get_template_content("webinterface.html"))
        # xmpp text
        bot_xmpp_button_text = "Start Wstbot"
        if self.process_running(self.wstbot_xmpp_proc):
            bot_xmpp_button_text = "Stop XMPP Wstbot"
        # server text
        server_button_text = "Start Wstbot Server"
        if self.process_running(self.wstbot_server_proc):
            server_button_text = "Stop Wstbot Server"
        # format
        page = template.substitute(bot_xmpp_button_text=bot_xmpp_button_text,
                                   server_button_text=server_button_text)
        return page

    def process_running(self, proc):
        if proc is not None:
            proc.poll()
            if proc.returncode is not None:
                return False
            else:
                return True
        else:
            return False

    def start_stop_process(self, proc, start):
        if self.process_running(proc):
            proc.terminate()
            return None
        else:
            return start()

    def toggle_wstbot_xmpp(self):
        self.wstbot_xmpp_proc = self.start_stop_process(self.wstbot_xmpp_proc, lambda : subprocess.Popen(WSTBOT_COMMAND))
        time.sleep(SLEEP_AMOUNT)

    def toggle_server(self):
        self.wstbot_server_proc = self.start_stop_process(self.wstbot_server_proc, lambda : subprocess.Popen(SERVER_COMMAND))
        time.sleep(SLEEP_AMOUNT)

def main():
    # start server
    server = WebInterface()
    cherrypy.quickstart(server, "/", server.server_config)
        
if __name__ == "__main__":
    main()
