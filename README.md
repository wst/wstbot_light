wstbot-light
============

Wstbot-light is a simple XMPP chat bot which records all links and displays them on a web server.

Dependencies
------------

* python 3.1+
* cherrypy
* sleekxmpp for xmpp

Installation
------------

1. Install the dependencies: ``pip3 install cherrypy`` and ``pip3 install sleekxmpp``
2. Rename wstbot.conf.EXAMPLE wstbot.conf and edit the values.
3. Run ``python3 setup.py`` to setup the database.

Usage
-----

Wstbot consists of 3 Parts:
* An XMPP chat bot based on sleekxmpp.
* A web application server based on cherrypy.
* A web interface that lets you start and stop the bot and the server.

Run the web interface with:

    python3 web_interface.py

Then visit http://localhost:8112/ in a web browser.
