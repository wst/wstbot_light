import os
import cherrypy
import mimetypes
import string
import random
from util import get_template_content, escape
from string import Template
from cherrypy.lib.static import serve_file
from constants import FILES_PATH

DEFAULT_FILE = "upload.html"
localDir = os.path.dirname(__file__)
absDir = os.path.join(os.getcwd(), localDir)

class Uploader:
    """File upload page"""
    
    @cherrypy.expose
    def index(self):
        html_template = get_template_content(DEFAULT_FILE)
        template = Template(html_template)        
        return template.substitute(actionUrl=(self.get_request_url() + "submit"))
        
    @cherrypy.expose
    def submit(self, file):
        url = self.get_request_url()
        url = url[0:url.find("submit")]
        
        filename = file.filename
        id_ = ""
        # if the plain filename is already in use we generate an id_ with at least 6 characters
        while os.path.isfile(os.path.join(absDir, FILES_PATH, filename)) and len(id_) < 6 or os.path.isfile(os.path.join(absDir, FILES_PATH, id_ + "_" + filename)):
            id_ += random.choice(string.ascii_letters)
        savedFile = open(os.path.join(absDir, FILES_PATH, (id_ + "_" if id_ else "") + filename), "wb")
        while True:
            data = file.file.read(8192)
            if not data:
                break          
            savedFile.write(data)
        savedFile.close()
        
        if id_:
            filename = id_ + "/" + filename
        out = '<div class="entry">'
        if (file.content_type.value.find("image") > -1): 
            out += '<img src="' + url + 'show/' + filename + '">'            
        out += '<label>'+ escape(file.filename) + '</label><br>\
                File link: <a href="' + url + 'show/' + filename + '">' + url + 'show/' + filename + '</a><br>\
                Download link: <a href="' + url + 'download/' + filename + '">' + url + 'download/' + filename + '</a><br>\
                Delete link: <a href="' + url + 'delete/' + filename + '">' + url + 'delete/' + filename + '</a>\
                </div>'
        return out
        
    @cherrypy.expose
    def show(self, id_, filename=None):
        if not filename:
            filename = id_
            id_ = None   
        return self.serve_file(id_, filename, False)
        
    @cherrypy.expose
    def download(self, id_, filename=None):
        if not filename:
            filename = id_
            id_ = None   
        return self.serve_file(id_, filename, True)
        
    @cherrypy.expose
    def delete(self, id_, filename=None):
        if not filename:
            filename = id_
            id_ = None            
        path = self.get_path(id_, filename)
        try:
            os.remove(path)
        except os.error as e:
            return "Error: File not found. %s" % e
        return "File succesfully deleted."
    
    def get_path(self, id_, filename):
        if id_:
            filename = id_ + "_" + filename
        return os.path.join(absDir, FILES_PATH, filename)
        
    def get_request_url(self):
        base = cherrypy.request.base
        path = cherrypy.request.path_info
        if not path.endswith("/"):
            path = path + "/"
        return base + path
    
    def serve_file(self, id_, filename, download=False):
        disposition = None
        path = self.get_path(id_, filename)
        mime =  mimetypes.guess_type(filename)[0]
        if (not mime): 
            mime = "application/octet-stream"
        if (download):
            disposition = "attachment"
        try:
            return serve_file(path, mime, disposition, filename, True)
        except cherrypy.NotFound:
            return "Error: File not found."