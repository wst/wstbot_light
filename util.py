import logging
import os
import re
import urllib.request
import importlib
import glob
import html.parser
import configparser
from constants import WEB_PATH, CONFIG_FILE

# regex bit to identify urls
URL_REGEX_PREFIX = r"(?:https?://)(?:www\.)?"

# maximum download size for websites (bytes)
WEB_READ_MAX = 1024 * 500 # 500kB
# encoding for websites
WEB_ENCODING = "utf-8"

class WstbotConfig:
    def __init__(self):
        parser = configparser.ConfigParser()
        parser.read(CONFIG_FILE)

        self.conn_account = parser.get("xmpp_connection", "account")
        self.conn_password = parser.get("xmpp_connection", "password")
        self.conn_room = parser.get("xmpp_connection", "room")
        self.conn_nick = parser.get("xmpp_connection", "nick")

        self.server_address = parser.get("server", "address")
        self.server_port = parser.getint("server", "port")
        self.server_url = "{}:{}".format(self.server_address, self.server_port)

        self.web_interface_port = parser.getint("web_interface", "port")
        self.web_interface_url = "{}:{}".format(self.server_address, self.web_interface_port)

def escape(message):
    return html.escape(message)

def unescape(message):
    parser = html.parser.HTMLParser()
    return parser.unescape(message)

def download_page_decoded(url):
    return download_page(url).decode(WEB_ENCODING)

def download_page(url):
    try:
        return urllib.request.urlopen(url).read(WEB_READ_MAX)
    except Exception as ex:
        logging.warning("Error opening url / downloading content: %s", str(ex))
        return None

def parse_for_url(message):
    """Try to find a URL in the message."""

    match = re.search(URL_REGEX_PREFIX + r"\S+", message)
    if match is not None:
        return match.group(0)
    return None

def first(sequence):
    """Returns the first encountered element in the sequence that is not None."""
    if sequence is None:
        return None
    for e in sequence:
        if e is not None:
            return e
    return None

def chain_call(value, functions):
    """Calls every function in functions with the value and returns the first result
    that is not None. Returns None if all functions returned None."""
    for f in functions:
        r = f(value)
        if r is not None:
            return r
    return None

def apply_seq(function, sequence):
    """Applies a function to every element of the sequence"""
    for e in sequence:
        function(e)

def str_list_to_int(l):
    """Converts a list of strings to a list of integers"""
    new_list = []
    for e in l:
        try:
            e = int(e)
        except:
            continue
        else:
            new_list.append(e)

    return new_list

def get_modules(path):
    """Return a list of python modules in a given path.
    Files not ending with '.py' are ignored.
    May throw an OSError."""

    modules = []

    module_path = path.replace(os.sep, ".")
    importlib.import_module(module_path)

    # get file names
    filenames = glob.glob(os.path.join(path, "*.py"))
    filenames = (os.path.basename(x) for x in filenames)
    # get module names
    module_names = (os.path.splitext(x)[0] for x in filenames)
    # import modules
    for module_name in module_names:
        # ignore __init__ and so on:
        if module_name[1] == "_":
            continue

        try:
            logging.info("Importing module '" + module_name + "'...")
            modules.append(importlib.import_module("." + module_name, module_path))
        except ImportError as err:
            logging.warning("Importing '%s' was unsuccessful!", module_path + "." + module_name)
            logging.warning("Reason: %s", err)

    return modules

def get_modules_objects(path, f=lambda x: x(), getter="get"):
    """Get all modules from a path (see get_modules), call a function
    in each of them and return its results. The getter function is 'get' by wstbot convention
    and the results are usually objects, therefore the name of this function.

    f will be used to call the getter function.

    If there is no getter function, look for a CLASS_ variable and try
    to instantiate it using f."""

    results = []
    modules = get_modules(path)
    for module in modules:
        # try to call the getter function
        func = getattr(module, getter, None)
        if func is not None:
            results.append(f(func))
        else:
            # try to instantiate the class
            class_ = getattr(module, "CLASS_", None)
            if class_ is not None:
                results.append(f(class_))
            else:
                logging.info("Omitting %s.", module.__name__)

    return results

def get_template_content(name):
    try:
        path = os.path.join(WEB_PATH, name)
        fp = open(path, "r")
        content = fp.read()
        fp.close()
        return content
    except IOError:
        return "File not found: " + name
